var expect = require('chai').expect,
    FunciList = require('../../../lib/Funci/List/FunciList');


describe('FunciList', function() {

    function lsList(list)
    {
        return list.ls();
    }

    it('should be initialisable', function() {
        var list;
        list = new FunciList();
        expect(list).to.be.an.instanceOf(FunciList);
    });

    describe('initialisation', function() {

        function lsListWith(arg)
        {
            return lsList(new FunciList(arg));
        }

        it('should contain no elements when constructed with null', function() {
            expect(lsListWith(null)).to.be.equal('');
        });

        it('should contain a single element when constructed with a boolean', function() {
            expect(lsListWith(true)).to.be.equal('true');
        });

        it('should contain a single element when constructed with a number', function() {
            expect(lsListWith(42)).to.be.equal('42');
        });

        it('should contain a single element when constructed with a string', function() {
            expect(lsListWith('foobar')).to.be.equal('foobar');
        });

        it('should contain a single element when constructed with an object', function() {
            expect(lsListWith({})).to.be.equal('[object Object]');
        });

        it('should contain array elements when constructed with an array', function() {
            expect(lsListWith([1, 2, 3])).to.be.equal('1, 2, 3');
        });

    });

    describe('primitive operations', function() {

        function primitiveOperationList()
        {
            return new FunciList([1, 2, 3]);
        }

        it('should return the head', function() {
            expect(lsList(primitiveOperationList().head())).to.be.equal('1');
        });

        it('should return the tail', function() {
            expect(lsList(primitiveOperationList().tail())).to.be.equal('2, 3');
        });

        it('should map over each element', function() {
            var squareFn;
            squareFn = function(item) {
                return item * item;
            };
            expect(lsList(primitiveOperationList().map(squareFn))).to.be.equal('1, 4, 9');
        })
    });

});