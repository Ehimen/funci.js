/**
 * The core List object in the Funci library.
 *
 * The list object is immutable, and every operation on this list will produce another list.
 *
 * @param arg How to construct the list.
 *
 * @constructor
 */
function FunciList(arg)
{
    var list;
    list = this;
    this._elements = [];
    if (null !== arg) {
        if (typeof arg === 'object' && typeof arg.forEach === 'function') {
            arg.forEach(function(item) {
                list._elements.push(item);
            })
        } else {
            this._elements.push(arg);
        }
    }
}

/**
 * Prints the list's contents as a string.
 *
 * @returns {string}
 */
FunciList.prototype.ls = function()
{
    return this._elements.join(', ');
};

/**
 * Returns a list containing the first element of this list.
 *
 * @returns {FunciList}
 */
FunciList.prototype.head = function()
{
    return this._clone(this._getAtPositionOrNull(0));
};


/**
 * Returns a list containing all non-head elements of this list.
 *
 * @returns {FunciList}
 */
FunciList.prototype.tail = function()
{
    return this._clone(this._elements.splice(1));
};

/**
 * Returns a list, after applying mapFn to every element of this list.
 *
 * @param mapFn
 *
 * @returns {FunciList}
 */
FunciList.prototype.map = function(mapFn)
{
    return this._clone(this._elements.map(mapFn));
};

FunciList.prototype._clone = function(arg)
{
    return new this.constructor(arg);
};

FunciList.prototype._getAtPositionOrNull = function(position)
{
    if (typeof this._elements[position] === 'undefined') {
        return null;
    }
    return this._elements[position];
};



module.exports = FunciList;